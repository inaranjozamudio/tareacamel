package com.curso.integracion.tarea.naranjo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SpringBootTareaCamel {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTareaCamel.class, args);
	}

}