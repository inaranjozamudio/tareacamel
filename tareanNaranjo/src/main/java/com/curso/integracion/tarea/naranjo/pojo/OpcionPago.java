package com.curso.integracion.tarea.naranjo.pojo;

import java.util.Date;

public class OpcionPago {

	private Integer opcionId;
	private Integer prefijoTarjeta;
	private String institutcionBancaria;
	private String tipoTarjeta;
	private String nombreBanco;
	private Integer estatus;
	private String tipoPago;
	private Integer monto3Meses;
	private Integer monto6Meses;
	private Integer monto12Meses;
	private Date creadoEn;
	private Integer cargaArchivo;
	private String exchangeId;
	public Integer getOpcionId() {
		return opcionId;
	}
	public void setOpcionId(Integer opcionId) {
		this.opcionId = opcionId;
	}
	public Integer getPrefijoTarjeta() {
		return prefijoTarjeta;
	}
	public void setPrefijoTarjeta(Integer prefijoTarjeta) {
		this.prefijoTarjeta = prefijoTarjeta;
	}
	public String getInstitutcionBancaria() {
		return institutcionBancaria;
	}
	public void setInstitutcionBancaria(String institutcionBancaria) {
		this.institutcionBancaria = institutcionBancaria;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getNombreBanco() {
		return nombreBanco;
	}
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	public Integer getEstatus() {
		return estatus;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public Integer getMonto3Meses() {
		return monto3Meses;
	}
	public void setMonto3Meses(Integer monto3Meses) {
		this.monto3Meses = monto3Meses;
	}
	public Integer getMonto6Meses() {
		return monto6Meses;
	}
	public void setMonto6Meses(Integer monto6Meses) {
		this.monto6Meses = monto6Meses;
	}
	public Integer getMonto12Meses() {
		return monto12Meses;
	}
	public void setMonto12Meses(Integer monto12Meses) {
		this.monto12Meses = monto12Meses;
	}
	public Date getCreadoEn() {
		return creadoEn;
	}
	public void setCreadoEn(Date creadoEn) {
		this.creadoEn = creadoEn;
	}
	public Integer getCargaArchivo() {
		return cargaArchivo;
	}
	public void setCargaArchivo(Integer cargaArchivo) {
		this.cargaArchivo = cargaArchivo;
	}
	public String getExchangeId() {
		return exchangeId;
	}
	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}
}
