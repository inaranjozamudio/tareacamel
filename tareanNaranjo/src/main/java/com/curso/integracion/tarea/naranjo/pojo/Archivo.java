package com.curso.integracion.tarea.naranjo.pojo;

import java.util.Date;

public class Archivo {

	private String idArchivo;
	private String nombre;
	private String createdAt;
	private String registrosProcesados;
	private String exchangeId;
	
	public Archivo(String idArchivo, String nombre, String createdAt, String registrosProcesados, String exchangeId) {
		super();
		this.idArchivo = idArchivo;
		this.nombre = nombre;
		this.createdAt = createdAt;
		this.registrosProcesados = registrosProcesados;
		this.exchangeId = exchangeId;
	}

}
