package com.curso.integracion.tarea.naranjo.entity;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import lombok.Data;

@CsvRecord(separator = ",", skipFirstLine = true )
@Data
public class OpcionesMeses {

	private Integer opcionId;
	private Integer prefijoTarjeta;
	private String institutcionBancaria;
	private String tipoTarjeta;
	private String nombreBanco;
	private Integer estatus;
	private String tipoPago;
	private Integer monto3Meses;
	private Integer monto6Meses;
	private Integer monto12Meses;
	private Date creadoEn;
	private Integer cargaArchivo;
	private String exchangeId;

	
	public OpcionesMeses(Integer opcionId, Integer prefijoTarjeta, String institutcionBancaria, String tipoTarjeta,
			String nombreBanco, Integer estatus, String tipoPago, Integer monto3Meses, Integer monto6Meses,
			Integer monto12Meses, Date creadoEn, Integer cargaArchivo, String exchangeId) {
		super();
		this.opcionId = opcionId;
		this.prefijoTarjeta = prefijoTarjeta;
		this.institutcionBancaria = institutcionBancaria;
		this.tipoTarjeta = tipoTarjeta;
		this.nombreBanco = nombreBanco;
		this.estatus = estatus;
		this.tipoPago = tipoPago;
		this.monto3Meses = monto3Meses;
		this.monto6Meses = monto6Meses;
		this.monto12Meses = monto12Meses;
		this.creadoEn = creadoEn;
		this.cargaArchivo = cargaArchivo;
		this.exchangeId = exchangeId;
	}
	
	
	
	
}

