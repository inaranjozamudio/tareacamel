package com.curso.integracion.tarea.naranjo.pojo;

import java.io.Serializable;
import java.util.Date;

public class GenericResponse implements Serializable{

	private Integer code;
	private boolean status;
	private String message;
	private Date date;
	public GenericResponse(Integer code, boolean status, String message, Date date) {
		super();
		this.code = code;
		this.status = status;
		this.message = message;
		this.date = date;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
