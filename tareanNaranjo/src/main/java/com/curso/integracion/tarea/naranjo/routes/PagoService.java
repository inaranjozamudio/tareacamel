package com.curso.integracion.tarea.naranjo.routes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.curso.integracion.tarea.naranjo.entity.OpcionesMeses;
import com.curso.integracion.tarea.naranjo.pojo.Archivo;
import com.curso.integracion.tarea.naranjo.pojo.GenericResponse;
import com.curso.integracion.tarea.naranjo.pojo.OpcionPago;


@Component
public class PagoService extends RouteBuilder{

	@Autowired
	DataSource datasource;

	DataFormat bindy = new BindyCsvDataFormat(OpcionesMeses.class);

	private final Environment env;
	public PagoService(Environment env) {
		this.env = env;
	}

	@Override
	public void configure() throws Exception {

		//GET ruta para cconsultar todas las opciones de pago
		from("direct:opcionesPagoAll")
		.log("direct:opcionesPagoAll")
		.setBody(constant("select * from integrator.opcionesMeses"))
		.to("jdbc:datasource")
		.process(this::getSqlDataPagos);

		from("direct:opcionesPagoByID")
		.process(this::querybuilderOpcionPago)
		.to("jdbc:dataSource")
		.process(this::getSqlDataPagos);
		
		from("direct:opcionesPagoDelete")
		.process(this::querybuilderOpcionPagoDelete)
		.to("jdbc:dataSource");


		//GET ruta para consultar los archicos cargados
		from("direct:archivosAll")
		.log("direct:archivosAll")
		.setBody(constant("select * from integrator.archivosProcesados"))
		.to("jdbc:datasource")
		.process(this::getSqlDataArchivos);

		from("direct:saveOpcionPago")
		.log("++++++++++++++direct:saveOpcionPago++++++++++++++")
		.process(this::saveSqlDataPagos)
		.to("jdbc:dataSource");
		
		from("direct:updateOpcionPago")
		.log("++++++++++++++direct:updateOpcionPago++++++++++++++")
		.process(this::updateSqlDataPagos)
		.to("jdbc:dataSource")
		.process(this::genericResponse);
	}


	@SuppressWarnings("unchecked")
	private void getSqlDataPagos(Exchange exchange) throws Exception {
		ArrayList<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) exchange.getIn().getBody();

		List<OpcionesMeses> datos = new ArrayList<OpcionesMeses>();
		datos =  dataList.stream()
				.map(m-> new OpcionesMeses(
						(Integer)m.get("opcionId"), 
						(Integer)m.get("prefijoTarjeta"), 
						(String)m.get("institutcionBancaria"), 
						(String)m.get("tipoTarjeta"), 
						(String)m.get("nombreBanco"), 
						(Integer)m.get("estatus"), 
						(String)m.get("tipoPago"), 
						(Integer)m.get("monto3Meses"), 
						(Integer)m.get("monto6Meses"), 
						(Integer)m.get("monto12Meses"), 
						(Date)m.get("creadoEn"), 
						(Integer)m.get("cargaArchivo"), 
						(String)m.get("exchangeId")
						)
						).collect(Collectors.toList());

		exchange.getIn().setBody(datos);

	}

	@SuppressWarnings("unchecked")
	private void getSqlDataArchivos(Exchange exchange) throws Exception {
		ArrayList<Map<String, String>> dataList = (ArrayList<Map<String, String>>) exchange.getIn().getBody();
		List<Archivo> usersList = new ArrayList<Archivo>();
		usersList =  dataList.stream()
				.map(m-> new Archivo(m.get("idArchivo"), m.get("nombre"), m.get("createdAt"), m.get("registrosProcesados"), m.get("exchangeId")))
				.collect(Collectors.toList());
		exchange.getIn().setBody(usersList);
	}

	private void saveSqlDataPagos(Exchange exchange) throws Exception {
		OpcionPago pojo = exchange.getIn().getBody(OpcionPago.class);
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO opcionesMeses(prefijoTarjeta,institutcionBancaria,tipoTarjeta,nombreBanco,estatus,tipoPago,monto3Meses,monto6Meses,monto12Meses,creadoEn,cargaArchivo,exchangeId)");
		sql.append("VALUES(");
		sql.append(pojo.getPrefijoTarjeta()).append(",");
		sql.append("'").append(pojo.getInstitutcionBancaria()).append("',");
		sql.append("'").append(pojo.getTipoTarjeta()).append("',");
		sql.append("'").append(pojo.getNombreBanco()).append("',");
		sql.append(pojo.getEstatus()).append(",");
		sql.append("'").append(pojo.getTipoPago()).append("',");
		sql.append(pojo.getMonto3Meses()).append(",");
		sql.append(pojo.getMonto6Meses()).append(",");
		sql.append(pojo.getMonto12Meses()).append(",");
		sql.append("now()").append(",");
		sql.append(pojo.getCargaArchivo()).append(",");
		sql.append("'").append(exchange.getExchangeId()).append("')");

		exchange.getIn().setBody(sql.toString());
	}


	private void querybuilderOpcionPago(Exchange exchange) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from integrator.opcionesMeses where opcionId =");
		sql.append(exchange.getIn().getHeader("id"));

		exchange.getIn().setBody(sql.toString());
	}
	
	private void querybuilderOpcionPagoDelete(Exchange exchange) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE from integrator.opcionesMeses where opcionId =");
		sql.append(exchange.getIn().getHeader("id"));

		exchange.getIn().setBody(sql.toString());
	}
	
	private void updateSqlDataPagos(Exchange exchange) throws Exception {
		OpcionPago pojo = exchange.getIn().getBody(OpcionPago.class);
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE opcionesMeses SET ");
		
		//TODO:PONER TODOS LOS CAMPOS EN EL SQL
		sql.append("institutcionBancaria = '").append(pojo.getInstitutcionBancaria()).append("' ");
		sql.append(" WHERE opcionId = ").append(pojo.getOpcionId());
		
		exchange.getIn().setBody(sql.toString());
	}
	
	
	private void genericResponse(Exchange exchange) throws Exception {
		GenericResponse pojo = new GenericResponse(200, true, "operacion realizada con exito", new Date());
		exchange.getIn().setBody(pojo);
	}
}
