package com.curso.integracion.tarea.naranjo.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.curso.integracion.tarea.naranjo.pojo.OpcionPago;


@Component
public class CamelApiController extends RouteBuilder{
	
	private final Environment env;
	public CamelApiController(Environment env) {
		this.env = env;
	}
	

	@Override
	public void configure() throws Exception {
		//Configuración de cajon
        restConfiguration()
        .contextPath(env.getProperty("camel.component.servlet.mapping.contextPath", "/rest/*"))
        .apiContextPath("/api-doc")
        .apiProperty("api.title", "Spring Boot Camel Postgres Rest API.")
        .apiProperty("api.version", "1.0")
        .apiProperty("cors", "true")
        .apiContextRouteId("doc-api")
        .port(env.getProperty("server.port", "8080"))
        .bindingMode(RestBindingMode.json);

		//Dar de alta las operaciones
		//Ruta para crear nuevas opciones de pago
		rest("/opcionesPago")
		.consumes(MediaType.APPLICATION_JSON_VALUE)
		.produces(MediaType.APPLICATION_JSON_VALUE)
		.get("/").route()
		.to("direct:opcionesPagoAll")
		.endRest()
		;

		rest("/opcionesPago")
		.consumes(MediaType.APPLICATION_JSON_VALUE)
		.produces(MediaType.APPLICATION_JSON_VALUE)
		.get("/{id}").route()
		.to("direct:opcionesPagoByID")
		.endRest()
		;
		
		
		rest("/opcionesPago")
		.consumes(MediaType.APPLICATION_JSON_VALUE)
		.produces(MediaType.APPLICATION_JSON_VALUE)
		.delete("/{id}").route()
		.to("direct:opcionesPagoDelete")
		.endRest()
		;
		
		
		rest("/opcionesPago/archivos")
		.consumes(MediaType.APPLICATION_JSON_VALUE)
		.produces(MediaType.APPLICATION_JSON_VALUE)
		.get("/").route()
		.to("direct:archivosAll")
		.endRest()
		;
		
		rest("/opcionesPago/")
		.consumes(MediaType.APPLICATION_JSON_VALUE)
		.produces(MediaType.APPLICATION_JSON_VALUE)
		.post("/").route()
		.marshal().json()
		.unmarshal(getJacksonDataFormat(OpcionPago.class))
		.to("direct:saveOpcionPago")
		.endRest()
		;
		
		rest("/opcionesPago/")
		.consumes(MediaType.APPLICATION_JSON_VALUE)
		.produces(MediaType.APPLICATION_JSON_VALUE)
		.put("/").route()
		.marshal().json()
		.unmarshal(getJacksonDataFormat(OpcionPago.class))
		.to("direct:updateOpcionPago")
		.endRest()
		;

	}
	
	 private JacksonDataFormat getJacksonDataFormat(Class<?> unmarshalType) {
	        JacksonDataFormat format = new JacksonDataFormat();
	        format.setUnmarshalType(unmarshalType);
	        return format;
	    }

}
